package com.modoo.unionsdk;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.saturn.sdk.framework.IAPUnion;
import com.saturn.sdk.framework.bean.PayInfo;
import com.saturn.sdk.framework.bean.UserInfo;
import com.saturn.sdk.framework.listener.LoginCallback;
import com.saturn.sdk.framework.listener.PayResult;
import com.saturn.sdk.framework.listener.RealNameResult;
import com.saturn.sdk.framework.listener.TimeLimitResult;
import com.saturn.sdk.framework.utils.LogUtil;
import com.tgcenter.unified.antiaddiction.api.AntiAddiction;
import com.tgcenter.unified.sdk.api.InitConfig;
import com.tgcenter.unified.sdk.api.TGCenter;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private TextView mInitTV;
    private TextView mVisitorTV;
    private TextView mWechatTV;
    private TextView mBindTV;
    private TextView mRealNameTV;
    private TextView mAntiTimeLimitDefaultTV;
    private TextView mAntiTimeLimitSelfTV;
    private TextView mRequestPayTV;
    private TextView mRoleInfoTV;
    private TextView mExitGameTV;

    private AlertDialog mAlertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        mInitTV = findViewById(R.id.iap_init_id);
        mInitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAPUnion.init(MainActivity.this);
                LogUtil.d(TAG, "user login status : " + IAPUnion.isLogin());
                LogUtil.d(TAG, "user bind status : " + IAPUnion.isBind());
            }
        });

        mVisitorTV = findViewById(R.id.iap_visitor_id);
        mVisitorTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAPUnion.loginWithVisitor(new LoginCallback() {
                    @Override
                    public void onLogin(boolean success, UserInfo userInfo, String reason) {
                        if (success) {
                            LogUtil.d(TAG, "user info : " + userInfo.toString());
                            LogUtil.d(TAG, "user bind status from user: " + userInfo.getBindStatus());
                            LogUtil.d(TAG, "user bind status : " + IAPUnion.isBind());
                        } else {
                            LogUtil.d(TAG, "user login failed: " + reason);
                        }
                    }

                    @Override
                    public void onExchange(boolean success, UserInfo userInfo) {

                    }

                    @Override
                    public void onLogout(boolean success, String msg) {

                    }
                });
            }
        });

        mWechatTV = findViewById(R.id.iap_wechat_id);
        mWechatTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAPUnion.loginWithWechat(new LoginCallback() {
                    @Override
                    public void onLogin(boolean success, UserInfo userInfo, String reason) {
                        if (success) {
                            LogUtil.d(TAG, "user info : " + userInfo.toString());
                            LogUtil.d(TAG, "user bind status from user: " + userInfo.getBindStatus());
                            LogUtil.d(TAG, "user bind status : " + IAPUnion.isBind());
                        } else {
                            LogUtil.d(TAG, "user login failed: " + reason);
                        }
                    }

                    @Override
                    public void onExchange(boolean success, UserInfo userInfo) {

                    }

                    @Override
                    public void onLogout(boolean success, String msg) {

                    }
                });
            }
        });

        mBindTV = findViewById(R.id.iap_bind_id);
        mBindTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAPUnion.bindWechat(new LoginCallback() {
                    @Override
                    public void onLogin(boolean success, UserInfo userInfo, String reason) {
                        if (success) {
                            LogUtil.d(TAG, "user info : " + userInfo.toString());
                            LogUtil.d(TAG, "user bind status from user: " + userInfo.getBindStatus());
                            LogUtil.d(TAG, "user bind status : " + IAPUnion.isBind());
                        } else {
                            LogUtil.d(TAG, "user login failed: " + reason);
                        }
                    }

                    @Override
                    public void onExchange(boolean success, UserInfo userInfo) {

                    }

                    @Override
                    public void onLogout(boolean success, String msg) {

                    }
                });
            }
        });

        mRealNameTV = findViewById(R.id.iap_real_name_id);
        mRealNameTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAPUnion.realName(getApplicationContext(), new RealNameResult() {
                    @Override
                    public void onSuccess(int status, int age) {
                        LogUtil.d(TAG, "RealName: status : " + status + ", age: " + age);
                    }

                    @Override
                    public void onFailed(int code, String message) {
                        LogUtil.d(TAG, "RealName: code : " + code + ", message: " + message);
                    }
                });
            }
        });

        mAntiTimeLimitDefaultTV = findViewById(R.id.iap_anti_time_limit_default_id);
        mAntiTimeLimitDefaultTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAPUnion.registerTimeLimitCallback(null);
            }
        });

        mAntiTimeLimitSelfTV = findViewById(R.id.iap_anti_time_limit_self_id);
        mAntiTimeLimitSelfTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAPUnion.registerTimeLimitCallback(new TimeLimitResult() {
                    @Override
                    public void onLimit(long limitTime, int type) {
                        LogUtil.d(TAG, "time limit , time : " + limitTime + " type: " + type);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mAlertDialog != null && mAlertDialog.isShowing()) {
                                    // 自定义dialog已显示
                                    LogUtil.d(TAG, "the self dialog is show");
                                } else {

                                }

                                String message = "";
                                int playType = 0; // 0 退出游戏，1 继续游戏
                                if (limitTime > 0) {
                                    message = String.format(getString(R.string.iap_self_limit_time_message),
                                            String.valueOf(limitTime / 1000));
                                    playType = 1;
                                } else {
                                    switch (type) {
                                        case TimeLimitResult.Child_20h_21h:
                                            message = getString(R.string.iap_self_limit_time_message_type_1);
                                            break;
                                        case TimeLimitResult.Child_Holiday_1_Hour:
                                            message = getString(R.string.iap_self_limit_time_message_type_2);
                                            break;
                                        case TimeLimitResult.Child_Normal_0_Hour:
                                            message = getString(R.string.iap_self_limit_time_message_type_3);
                                            break;
                                    }
                                    playType = 0;
                                }
                                int finalPlayType = playType;
                                mAlertDialog = new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("自定义游戏时长限制")
                                        .setMessage(message)
                                        .setNegativeButton(playType == 0 ? "退出游戏" : "继续游戏", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (finalPlayType == 0) {
                                                    System.exit(-1);
                                                } else {
                                                    // do nothing
                                                }
                                            }
                                        }).create();
                                mAlertDialog.show();
                            }
                        });
                    }
                });
            }
        });

        mRequestPayTV = findViewById(R.id.iap_request_pay_id);
        mRequestPayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PayInfo payInfo = new PayInfo();
                payInfo.setPropsId("1");
                payInfo.setOrderId("11332446923161527530");
//                payInfo.setOrderId("test_order_" + getPackageName() + "_" + System.currentTimeMillis());
                payInfo.setProductName("青釭剑*1");
                payInfo.setOrderPrice(0.01f);
                payInfo.setProductDescription("购买青釭剑一把");
                payInfo.setProductNumber(1);
                // 这个根据服务端返回url进行填写
                payInfo.setNotifyUrl("www.baidu.com");
                // 该字段为测试字段，实际使用时直接透传服务端返回信息
                String testExtInfo = "{\n" +
                        "\t\"uid\": 123456\n" +
                        "}";
                payInfo.setExtInfo(testExtInfo);
                IAPUnion.requestPay(payInfo, new PayResult() {
                    @Override
                    public void onSuccess() {
                        LogUtil.d(TAG, "Pay success");
                    }

                    @Override
                    public void onCancel() {
                        LogUtil.d(TAG, "Pay cancel");
                    }

                    @Override
                    public void onFailed(int code, String message) {
                        LogUtil.d(TAG, "Pay onFailed, code is " + code + " , message is " + message);
                    }
                });
            }
        });

        mRoleInfoTV = findViewById(R.id.iap_role_id);
        mRoleInfoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAPUnion.submitRoleInfo(null);
            }
        });

        mExitGameTV = findViewById(R.id.iap_exit_game_id);
        mExitGameTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAPUnion.exitGame(null);
            }
        });
        initTG();
    }

    private void initTG() {
        TGCenter.init(MainActivity.this,
                InitConfig.newBuilder()
                        // 在测试阶段，应用可以开启调试日志，来查看各 SDK 是否正确运行
                        .setDebugMode(BuildConfig.DEBUG)
                        // 渠道：可选，用于传入 Umeng、RangersAppLog SDK
                        .build());
        AntiAddiction.getInstance().setAutoShowTimeLimitPage(false);
    }
}