package com.modoo.unionsdk;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import androidx.annotation.NonNull;

import com.saturn.sdk.framework.IAPApp;

public class TestApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        IAPApp.onCreate(this);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        IAPApp.onConfigurationChanged(newConfig);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        IAPApp.attachBaseContext(base);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        IAPApp.onTerminate();
    }
}
